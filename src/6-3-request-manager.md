# Menedżer zleceń

Implementację menedżera zleceń rozpoczniemy od zdefiniowania samego zlecenia. W tym celu w osobnym pliku stworzymy nowy moduł o nazwie `request`:

```rust
pub type RequestId = u32;

pub enum RequestType {
    Overview,
    OilChange,
    TyresChange { buy_new: bool },
    Repair(String),
}

type ClientId = u32;

pub struct Request {
    id: RequestId,
    client_id: ClientId,
    request_type: RequestType,
    creation_time: u64,
}
```

Utworzyliśmy typ wyliczeniowy `RequestType`, za pomocą którego będziemy mogli klasyfikować różne rodzaje zleceń (tutaj: przegląd samochodowy, wymiana oleju, wymiana opon, naprawa). Wariant `TyresChange` dodatkowo przechowuje parametr `buy_new` typu `bool`, za pomocą którego możemy przechować informację o tym, czy klient chce zakupić nowe opony. Wariant `Repair` jest bardzo ogólny, dlatego wymaga dodatkowego opisu. Opis ten zawarty jest w anonimowym parametrze typu `String`.

Struktura samego zlecenia składa się z kilku pól: unikatowego identyfikatora zlecenia, unikatowego identyfikatora klienta, rodzaju zlecenia oraz czasu złożenia zlecenia. Chwilowo do zapisania czasu użyliśmy typu `u64`. Jest to jednak bardzo mało czytelne, ponieważ nie możemy wprost stwierdzić w jakiej konwencji przechowywany jest czas. Aby to poprawić, skorzystamy z zewnętrznej biblioteki `chrono`. Więcej informacji na temat tej biblioteki znaleźć można [tutaj](https://crates.io/crates/chrono) oraz [tutaj](https://docs.rs/chrono/latest/chrono/).

Zmodyfikujmy plik `Cargo.toml` tak, aby uwzględnić zewnętrzną bibliotekę:

```ini
[package]
name = "car-workshop"
version = "0.4.0"
edition = "2021"

[dependencies]
chrono = "0.4"
```

Natomist w pliku `request.rs` wprowadźmy dwie zmiany: dodajmy na samej górze:

```rust
use chrono::prelude::*;
```

oraz zmieńmy definicję struktury `Request` na:

```rust
pub struct Request {
    id: RequestId,
    client_id: ClientId,
    creation_time: DateTime<Utc>,
    request_type: RequestType,
}
```

Zamiast `u64` użyliśmy typu `DateTime<Utc>`, który dostępny jest w preludium biblioteki `chrono`. UTC to uniwersalny czas koordynowany (ang. Universal Time Coordinated). Zamiast niego mogliśmy użyć `Local` - wówczas przechowywalibyśmy datę i czas lokalny.

Ponieważ pola struktury `Request` są prywatne, musimy dodać jakiś sposób, by tworzyć nowe instancje takiego zlecenia. Dodajmy w pierwszej kolejności test jednostkowy hipotetycznej funkcji `new()`:

```rust
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let now = Utc::now();
        let request_id = 7;
        let client_id = 10;
        let request_type = RequestType::Overview;
        let request = Request::new(request_id, client_id, request_type);
        assert!(request.creation_time >= now);
    }
}
```

A teraz implementacja samej funkcji:

```rust
impl Request {
    pub fn new(id: RequestId, client_id: ClientId, request_type: RequestType) -> Self {
        Self {
            id,
            client_id,
            request_type,
            creation_time: Utc::now(),
        }
    }
}
```

Gdy wywołamy `cargo test`, `cargo` samo pobierze bibliotekę `chrono` w odpowiedniej wersji. Czas użyć naszej nowej struktury w menedżerze!

## Listy zleceń 

Zlecenia, które otrzymuje warsztat samochodowy mogą mieć jeden z trzech statusów: oczekujące, w trakcie realizacji, zakończone. W powszechnie stosowanym podejściu rozszerzylibyśmy strukturę `Request` o taki status, ale wówczas lista wszystkich zleceń by się wymieszała i przy każdej okazji musielibyśmy przeszukiwać wszystkie zlecenia, bez względu na status. Zamiast tego zmodyfikujemy strukturę `RequestManager` stosując podejście DOD (ang. Data Oriented Design): 

```rust
mod request;

use chrono::prelude::*;
use request::Request;

type MechanicId = u32;

pub struct RequestManager {
    awaiting_requests: Vec<Request>,
    ongoing_requests: Vec<(Request, MechanicId)>,
    finished_requests: Vec<(Request, DateTime<Utc>)>,
}

impl RequestManager {
    pub fn new() -> Self {
        Self {
            awaiting_requests: Vec::new(),
            ongoing_requests: Vec::new(),
            finished_requests: Vec::new(),
        }
    }
}
```

Do przechowywania zleceń o różnym statusie użyliśmy trzech niezależnych kolekcji. Pole `awaiting_requests` (zlecenia oczekujące) posiada jedynie instancje struktury `Request`. Pole `ongoing_requests` (zlecenia w trakcie realizacji) zostało dodatkowo rozszerzone o identyfikator mechanika, który został do niego przydzielony. Pole `finished_requests` (zlecenia ukończone) zostało dodatkowo rozszerzone o czas ukończenia zlecenia. 

Nie zapomnijmy napisać testu!

```rust
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let manager = RequestManager::new();
        assert_eq!(manager.awaiting_requests.len(), 0);
        assert_eq!(manager.ongoing_requests.len(), 0);
        assert_eq!(manager.finished_requests.len(), 0);
    }
}
```

## Dodawanie nowych zleceń

Przejdźmy teraz do funkcjonalności, które udostępnia menedżer zleceń. W pierwszej kolejności musi on pozwalać na dodawanie nowych zleceń. Zaczniemy od napisania testu:

```rust
#[test]
fn test_add_new_request() {
    let mut manager = RequestManager::new();

    let client_id = 13;
    let request_type = RequestType::Repair(String::from("The battery does not charge."));
    let mechanic_id = None;
    manager.add_new_request(client_id, request_type, mechanic_id);
    assert_eq!(manager.awaiting_requests.len(), 1);

    let client_id = 145;
    let request_type = RequestType::TyresChange { buy_new: true };
    let mechanic_id = Some(18);
    manager.add_new_request(client_id, request_type, mechanic_id);
    assert_eq!(manager.ongoing_requests.len(), 1);
}
```

Hipotetyczna metoda `add_new_request()` przyjmuje identyfikator klienta, rodzaj zlecenia oraz opcjonalnie przydzielonego mechanika. Jeśli mechanik został przydzielony, zlecenie trafia od razu do listy zleceń będących w trakcie realizacji. Jeśli nie, zlecenie czeka na listę zleceń oczekujących. Przejdźmy teraz do implementacji:

```rust
// ...

type ClientId = u32;

//...

pub fn add_new_request(
    &mut self,
    client_id: ClientId,
    request_type: RequestType,
    mechanic_id: Option<MechanicId>,
) -> RequestId {
    let id = 0;
    let request = Request::new(id, client_id, request_type);
    if let Some(mechanic_id) = mechanic_id {
        self.ongoing_requests.push((request, mechanic_id));
    } else {
        self.awaiting_requests.push(request);
    }
    id
}
```

Skorzystaliśmy z wyrażenia `if let Some(mechanic_id) = mechanic_id { ... }`, które jest bardzo przydatne, gdy chcemy sprawdzić typ wyliczeniowy tylko pod kątem jednej wartości. Użyliśmy dodatkowo zacieniania, bo nowa zmienna `mechanic_id` przykryła wcześniejszą zmienną. Wyłuskaliśmy wartość z wariantu `Some()`, ale nie musieliśmy dodawać nowej nazwy. 

## Generowanie identyfikatorów

Podobnie jak w przypadku menedżera mechaników, również menedżer zleceń potrzebuje czegoś, co pozwoli mu generować unikatowe identyfikatory. Ponieważ mamy już taki mechanizm, przeniesiemy go w bardziej dostępne miejsce tak, aby inne moduły również mogły z niego korzystać. W tym celu utwórzmy nowy katalog `src/car_workshop/utilities/`, a następnie przenieśmy do niego plik `src/car_workshop/mechanic_manager/id_generator.rs`.

```console
$ mkdir src/car_workshop/utilities
$ mv src/car_workshop/mechanic_manager/id_generator.rs src/car_workshop/utilities
```

W pliku `car_workshop/mod.rs` musimy zadeklarować istnienie podrzędnego modułu `id_generator`, a ponieważ znajduje się on wewnątrz katalogu `utilities/`, deklaracja wygląda tak:

```rust
mod utilities {
    pub mod id_generator;
}
```

Musieliśmy dodać słówko `pub`, ponieważ inaczej byłby on prywatnym modułem dla modułu `utilities`. Aby ułatwić sprawę innym podrzędnym modułom, dodamy strukturę `IdGenerator` do przestrzeni nazw:

```rust
use utilities::id_generator::IdGenerator;
```

Teraz, w module `mechanic_manager` możemy wprowadzić następujące zmiany:

```rust
mod mechanic;

use super::IdGenerator;
use mechanic::{Mechanic, MechanicId};
```

Usunęliśmy wcześniejszą deklarację `mod id_generator`, ponieważ nie jest to już moduł podrzędny dla modułu `mechanic_manager`. Dodatkowo zaimportowaliśmy symbol `IdGenerator` z nadrzędnego modułu (`super`). Wszystko powinno się zbudować.

Tak wygląda nasz menedżer zleceń po dodaniu własnego generatora identyfikatorów:

```rust
mod request;

use super::IdGenerator;
use chrono::prelude::*;
use request::{Request, RequestId, RequestType};

type ClientId = u32;
type MechanicId = u32;

pub struct RequestManager {
    awaiting_requests: Vec<Request>,
    ongoing_requests: Vec<(Request, MechanicId)>,
    finished_requests: Vec<(Request, DateTime<Utc>)>,
    id_generator: IdGenerator,
}

impl RequestManager {
    pub fn new() -> Self {
        Self {
            awaiting_requests: Vec::new(),
            ongoing_requests: Vec::new(),
            finished_requests: Vec::new(),
            id_generator: IdGenerator::new(),
        }
    }

    pub fn add_new_request(
        &mut self,
        client_id: ClientId,
        request_type: RequestType,
        mechanic_id: Option<MechanicId>,
    ) -> RequestId {
        let id = self.id_generator.get_new_id();
        let request = Request::new(id, client_id, request_type);
        if let Some(mechanic_id) = mechanic_id {
            self.ongoing_requests.push((request, mechanic_id));
        } else {
            self.awaiting_requests.push(request);
        }
        id
    }
}
```

## Finalizowanie zleceń

Kolejną istotną funkcjonalnością jest możliwość finalizowania zleceń. Zacznijmy od testu:

```rust
    #[test]
    fn test_finish_request() {
        let mut manager = RequestManager::new();

        let request_id = 5;
        manager
            .awaiting_requests
            .push(Request::new(request_id, 10, RequestType::OilChange));

        let free_mechanic = manager.finish_request(request_id);
        assert_eq!(free_mechanic, None);
        assert_eq!(manager.awaiting_requests.len(), 0);
        assert_eq!(manager.finished_requests.len(), 1);

        let request_id = 10;
        let mechanic_id = 88;
        manager.ongoing_requests.push((
            Request::new(request_id, 20, RequestType::Overview),
            mechanic_id,
        ));

        let free_mechanic = manager.finish_request(request_id);
        assert_eq!(free_mechanic, Some(mechanic_id));
        assert_eq!(manager.ongoing_requests.len(), 0);
        assert_eq!(manager.finished_requests.len(), 2);
    }
```

A oto implementacja:

```rust
    pub fn finish_request(&mut self, id: RequestId) -> Option<MechanicId> {
        let mut mechanic_id = None;
        self.take_awaiting_request(id)
            .or(self.take_ongoing_request(id).map(|(request, mech_id)| {
                mechanic_id = Some(mech_id);
                request
            }))
            .and_then(|request| Some(self.finished_requests.push((request, Utc::now()))));
        mechanic_id
    }

    fn take_awaiting_request(&mut self, id: RequestId) -> Option<Request> {
        self.awaiting_requests
            .iter()
            .position(|request| request.id == id)
            .and_then(|index| Some(self.awaiting_requests.swap_remove(index)))
    }

    fn take_ongoing_request(&mut self, id: RequestId) -> Option<(Request, MechanicId)> {
        self.ongoing_requests
            .iter()
            .position(|(request, _)| request.id == id)
            .and_then(|index| Some(self.ongoing_requests.swap_remove(index)))
    }
```

Dodaliśmy dwie prywatne metody służące do wyciągania obiektów z poszczególnych kolejek, a następnie użyliśmy ich w sposób łańcuchowy. Zobaczmy, czy nasze testy przechodzą:

```console
$ cargo test
   Compiling car-workshop v0.4.0 (/home/osboxes/Workspace/rust-tutorial/code/car-workshop)
    Finished test [unoptimized + debuginfo] target(s) in 0.49s
     Running unittests (target/debug/deps/car_workshop-b4a3fa63d55c5b4d)

running 9 tests
test car_workshop::mechanic_manager::test::test_fire ... ok
test car_workshop::mechanic_manager::test::test_hire ... ok
test car_workshop::mechanic_manager::test::test_new ... ok
test car_workshop::request_manager::request::test::test_new ... ok
test car_workshop::request_manager::test::test_add_new_request ... ok
test car_workshop::request_manager::test::test_finish_request ... ok
test car_workshop::request_manager::test::test_new ... ok
test car_workshop::utilities::id_generator::test::test_get_new_id ... ok
test car_workshop::utilities::id_generator::test::test_new ... ok

test result: ok. 9 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
```

Wygląda obiecująco!
