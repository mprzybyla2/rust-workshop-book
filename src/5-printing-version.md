# Wyświetlanie wersji

Rozpoczniemy od czegoś bardzo prostego - chcemy, aby nasza aplikacja wyświetliła wiadomość powitalną, zawierającą wersję projektu. Zastosujemy [wersjonowanie semantyczne](https://semver.org/lang/pl/). W tym celu zmodyfikujemy plik `main.rs`:

```rust
fn main() {
    println!("Car Workshop ver. {}.{}.{}", 0, 1, 0);
}
```

Możemy również posłużyć się taką formą:

```rust
fn main() {
    println!(
        "Car Workshop ver. {major}.{minor}.{patch}",
        major = 0,
        minor = 1,
        patch = 0
    );
}
```

(Nawiasem mówiąc, Visual Studio Code udostępnia skrót klawiszowy `Shift + Alt + F` lub `Ctrl + Shift + I`, dzięki któremu możemy w łatwy sposób sformatować bieżący dokument.)

## Krotki

Gdybyśmy chcieli przechować dane o wersji w jakiejś zmiennej, możemy posłużyć się krotką (ang. tuple):

```rust
fn main() {
    let version = (0, 1, 0);
    println!(
        "Car Workshop ver. {}.{}.{}",
        version.0, version.1, version.2
    );
}
```

Krotki przydają się szczególnie gdy zwracamy kilka obiektów z funkcji. Spróbujmy 

```rust
fn main() {
    let version = get_version();
    println!(
        "Car Workshop ver. {}.{}.{}",
        version.0, version.1, version.2
    );
}

fn get_version() -> (u32, u32, u32) {
    (0, 1, 0)
}
```

Indeksowanie elementów krotki (`.0, .1`) nie jest szczególnie czytelne, dlatego lepiej posłużyć się destrukturyzacją krotki:

```rust
fn main() {
    let (major, minor, patch) = get_version();
    println!("Car Workshop ver. {}.{}.{}", major, minor, patch);
}

fn get_version() -> (u32, u32, u32) {
    (0, 1, 0)
}
```

## Struktury 

Możemy definiować także struktury opierające się na krotce. Wówczas z funkcji przekazujemy strukturę, którą nadal można zdestrukturyzować:

```rust
fn main() {
    let Version(major, minor, patch) = get_version();
    println!("Car Workshop ver. {}.{}.{}", major, minor, patch);
}

struct Version(u32, u32, u32);

fn get_version() -> Version {
    Version(0, 1, 0)
}
```

Nazwa struktury opartej o krotkę nadaje jej pewne znaczenie, które pomaga czytać kod. Np. `(0, 255, 0)` niewiele znaczy, ale `Rgb(0, 255, 0)` już tak. Ale skoro zależy nam na czytelności kodu, użyjmy struktury z nazwanymi polami:

```rust
fn main() {
    let version = get_version();
    println!(
        "Car Workshop ver. {}.{}.{}",
        version.major, version.minor, version.patch
    );
}

struct Version {
    major: u32,
    minor: u32,
    patch: u32,
}

fn get_version() -> Version {
    Version {
        major: 0,
        minor: 1,
        patch: 0,
    }
}
```

(Zwróć uwagę na przecinki stawiane także po ostatnich polach występujących zarówno w definicji jak i w inicjalizacji struktury. Nie są one wymagane, ale standardowy formater je dodaje.)

## Metody struktur

Wolnostojąca funkcja `get_version()` jest przydatna jedynie w kontekście typu `Version`, dlatego zamienimy ją na funkcję powiązaną (ang. associated function).

```rust
fn main() {
    let version = Version::get();
    println!(
        "Car Workshop ver. {}.{}.{}",
        version.major, version.minor, version.patch
    );
}

struct Version {
    major: u32,
    minor: u32,
    patch: u32,
}

impl Version {
    fn get() -> Self {
        Self {
            major: 0,
            minor: 1,
            patch: 0,
        }
    }
}
```

Blok oznaczony przez `impl Version` otacza funkcje powiązane oraz metody związane z typem `Version`. Słówko kluczowe `Self` to alias typu, dla którego implementujemy funkcję. Do nowo napisanej funkcji odnosimy się poprzez `Version::get()`.

## Pobieranie wersji z `cargo`

Kiedy korzystamy z `cargo`, wersja projektu dostępna jest w pliku `Cargo.toml`, więc duplikowanie wersji w kodzie łamie zasadę DRY. Kompilator otrzymuje od `cargo` szereg zmiennych środowiskowych, z których możemy skorzystać. Nas konkretnie interesują zmienne: `CARGO_PKG_VERSION_MAJOR`, `CARGO_PKG_VERSION_MINOR` oraz `CARGO_PKG_VERSION_PATCH`. Aby się do nich dostać skorzystamy z makra `env!()`:

```rust
impl Version {
    fn get() -> Self {
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();

        Self {
            major,
            minor,
            patch,
        }
    }
}
```

Makro `env!()` zwraca typ `&str`, czyli tzw. string slice (podgląd na stringa). Aby przekonwertować go na liczbę używamy generycznej funkcji `parse()`. Ponieważ parsowanie mogło się powieść lub nie, metoda `parse()` zwraca generyczny typ `Result<T, E>`, który zawiera wynik lub komunikat błędu. Metoda `unwrap()` wyłuskuje wynik, ignorując błąd.

Zwróć uwagę, że kompilator sam wydedukował typ zmiennych `major`, `minor,` `patch` w oparciu o ich późniejsze zastosowanie (przypisanie do pól struktury `Version`). Dzięki temu nie musieliśmy do parsowania podawać pełnoprawnej nazwy funkcji `parse::<u32>()`. Późna dedukcja typów to potężne narzędzie, które znacząco upraszcza kod.

Zwróć także uwagę na inicjalizację struktury. Ponieważ nazwy zmiennych odpowiadają nazwom pól struktury, nie musieliśmy jawnie pisać `major: major`. Takie przypisanie jest także odporne na niewłaściwą kolejność, a zatem taki zapis również poprawnie zainicjalizuje pola:

```rust
Self {
    minor,
    major,
    patch,
}
```

## Wyświetlanie struktury

Nasza funkcja `main()` wygląda obecnie tak:

```rust
fn main() {
    let version = Version::get();
    println!(
        "Car Workshop ver. {}.{}.{}",
        version.major, version.minor, version.patch
    );
}
```

Chcielibyśmy uprościć ją do takiej formy:

```rust
fn main() {
    println!("Car Workshop ver. {}", Version::get());
}
```

W chwili obecnej to się nie skompiluje, a błąd kompilatora to:

```console
error[E0277]: `Version` doesn't implement `std::fmt::Display`
```

Aby można było użyć jakiegokolwiek typu w kontekście formatowania tekstu (`"{}"`), typ ten musi _implementować cechę_ `std::fmt::Display`. Cechy to pewnego rodzaju interfejsy, które musi implementować typ, aby można go było użyć w różnych kontekstach. Np. typ musi implementować cechę `std::ops::Add`, aby można go było dodawać operatorem `+`. Zanim zaimplementujemy cechę `std::fmt::Display`, posłużymy się pewnym tanim trikiem.

### Cecha Debug

Zamiast cechy `std::fmt::Display`, zaimplementujemy w sposób automatyczny cechę `std::fmt::Debug`. Jest to cecha, która umożliwia wyświetlanie tekstu w specjalnym trybie formatowania tekstu `"{:?}"`.

```rust
fn main() {
    println!("Car Workshop ver. {:?}", Version::get());
}

#[derive(Debug)]
struct Version {
    major: u32,
    minor: u32,
    patch: u32,
}

impl Version {
    fn get() -> Self {
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();

        Self {
            major,
            minor,
            patch,
        }
    }
}
```

Zwróć uwagę na linię `#[derive(Debug)]`. Jest to inny rodzaj makra, który w sposób automatyczny implementuje (`derive`) cechę `Debug` dla danego typu (cecha `Debug`, tak jak wiele innych elementów biblioteki standardowej jest dostępna w tzw. preludium, dlatego nie musimy pisać pełnej ścieżki `std::fmt::Debug`). Gdy uruchomimy program (ignorując chwilowo ostrzeżenia) zobaczymy w wyniku:

```console
Car Workshop ver. Version { major: 0, minor: 1, patch: 0 }
```

Cała definicja struktury wraz z wartościami pól została wypisana do konsoli. Nie jest to najpiękniejszy styl, ale dla celów debugowych takie szybkie umożliwienie wyświetlania może okazać się pomocne.

### Cecha Display

W odróżnieniu od `std::fmt::Debug`, cechy `std::fmt::Display` nie można dodać do typu w sposób automatyczny, lecz trzeba ją ręcznie zaimplementować. Zrobimy to teraz:

```rust
impl std::fmt::Display for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}
```

Implementacja cechy dla danego typu polega na dodaniu do niego wymaganych metod w bloku `impl <nazwa cechy> for <nazwa typu>`. Jedyna metoda, której wymaga cecha `std::fmt::Display`, to `fmt()`. Parametr `&self` oznacza, że metoda pożycza w sposób niemodyfikowalny daną instancję typu (odpowiednikiem w C++ jest `this` w metodzie typu `const`). Parametr `&mut std::fmt::Formatter` oznacza, że metoda pożycza w sposób modyfikowalny ujście, do którego chcemy wypisać nasze dane.

Makro `write!()` działa podobnie jak `println!()`, ale zamiast wypisywać informację na konsolę, przekazuje je jedynie dalej do ujścia `f`. Zwróć uwagę na konieczność podawania `self`, aby dostać się do pól instancji. 

Nawiasem mówiąc implementacja cechy `std::fmt::Display` w sposób automatyczny nadaje też typowi cechę `std::string::ToString`, która udostępnia metodę `to_string()`, konwertującą dany typ do typu `std::string::String`.

## Rozszerzanie przestrzni nazw o zewnętrzne elementy 

Możemy uprościć nieco zapis tej implementacji, dodając do przestrzeni nazw poszczególne elementy z modułu `std::fmt`. 

```rust
use std::fmt::{Display, Formatter, Result};

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}
```

Taki zapis wydaje się sporo bardziej przejrzysty. W chwili obecnej cały nasz kod wygląda następująco:

```rust
use std::fmt::{Display, Formatter, Result};

fn main() {
    println!("Car Workshop ver. {}", Version::get());
}

struct Version {
    major: u32,
    minor: u32,
    patch: u32,
}

impl Version {
    fn get() -> Self {
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();

        Self {
            major,
            minor,
            patch,
        }
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}
```

## Wydzielanie osobnego modułu

Do organizacji kodu Rust stosuje tzw. moduły. W jednym pliku może istnieć wiele modułów, ale dobrym nawykiem jest wydzielanie dla nich osobnych plików. Dla celów dydaktycznych zastosujemy obydwa sposoby. W rezultacie, w pliku `main.rs` chcielibmyśmy mieć tylko funkcję `main()` oraz deklarację modułów. 

### Moduł w tym samym pliku

W pierwszej kolejności zbierzemy wszystkie części kodu związane z typem `Version` i umieścimy je w module o nazwie `version`:

```rust
fn main() {
    println!("Car Workshop ver. {}", version::Version::get());
}

mod version {
    use std::fmt::{Display, Formatter, Result};

    struct Version {
        major: u32,
        minor: u32,
        patch: u32,
    }

    impl Version {
        fn get() -> Self {
            let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
            let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
            let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();

            Self {
                major,
                minor,
                patch,
            }
        }
    }

    impl Display for Version {
        fn fmt(&self, f: &mut Formatter) -> Result {
            write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
        }
    }
}
```

Moduł `version` enkapsuluje kod związany z wersją oprogramowania, dlatego w funkcji `main()` musieliśmy podać pełnoprawną ścieżkę `version::Version::get()`. Gdybyśmy jednak spróbowali skompilować ten kod, otrzymamy dwa błędy:

```console
error[E0603]: struct `Version` is private
error[E0624]: associated function `get` is private
```

Domyślnie wszystkie składniki modułu są prywatne dla zewnątrz. Jeśli chcemy je upublicznić, musimy skorzystać ze słówka kluczowego `pub`.

```rust
    pub struct Version {
    /// ...
        pub fn get() -> Self {
```

### Moduł w wydzielonym pliku

Utwórzmy nowy plik `version.rs` i przenieśmy do niego zawartość modułu `version`:

```rust
use std::fmt::{Display, Formatter, Result};

pub struct Version {
    major: u32,
    minor: u32,
    patch: u32,
}

impl Version {
    pub fn get() -> Self {
        let major = env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap();
        let minor = env!("CARGO_PKG_VERSION_MINOR").parse().unwrap();
        let patch = env!("CARGO_PKG_VERSION_PATCH").parse().unwrap();

        Self {
            major,
            minor,
            patch,
        }
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}
```

Zauważ, że nie jest potrzebne otaczanie kodu blokiem `mod version {}` - plik sam z siebie stanowi zakres modułu, a nazwa pliku stanowi jego nazwę. Aby w pliku `main.rs` zaimportować moduł z osobnego pliku, należy użyć:

```rust
mod version;

fn main() {
    println!("Car Workshop ver. {}", version::Version::get());
}
```

Dzięki temu przenieśliśmy detale pobierania i odczytywania wersji naszej aplikacji do osobnego pliku i nie musimy już więcej na nie patrzeć.

Możemy jeszcze nieco poprawić nasz kod dodając typ `Version` do przestrzeni nazw w pliku `main.rs`:

```rust
mod version;
use version::Version;

fn main() {
    println!("Car Workshop ver. {}", Version::get());
}
```

Na koniec nie pozostaje nam nic innego jak podbić wersję minor w pliku `Cargo.toml`:

```ini
[package]
name = "car-workshop"
version = "0.2.0"
edition = "2021"
```

Sprawdźmy, czy wszystko działa:

```console
$ cargo run
   Compiling car-workshop v0.2.0 (/home/osboxes/car-workshop)
    Finished dev [unoptimized + debuginfo] target(s) in 0.33s
     Running `target/debug/car-workshop`
Car Workshop ver. 0.2.0
```

Brawo! 
