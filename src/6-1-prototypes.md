# Prototypy

Zaczniemy implementację odgórnie (ang. top-to-bottom), implementując jedynie prototypy modułów, które zakładamy, że powinny działać w systemie.

## Moduł główny

W pierwszej kolejności dodajmy duży moduł `car_workshop`, który będzie zawierał całą logikę biznesową. Tym razem, zamiast tworzyć dla tego modułu osobny plik, utworzymy cały osobny katalog. Jeśli w danym katalogu istnieje plik `mod.rs`, to nazwa tego katalogu określa nazwę modułu, a zawartość pliku `mod.rs` określa zawartość modułu. Zobaczmy to w praktyce:

```console
$ mkdir src/car_workshop
$ touch src/car_workshop/mod.rs
```

Wewnątrz katalogu dodajmy w pierwszej kolejności pustą strukturę wraz z pojedynczą funkcją powiązaną:

```rust
pub struct CarWorkshop {}

impl CarWorkshop {
    pub fn new() -> Self {
        CarWorkshop {}
    }
}
```

Następnie w pliku `main.rs` spróbujmy użyć tego modułu:

```rust
mod version;
use version::Version;

mod car_workshop;
use car_workshop::CarWorkshop;

fn main() {
    println!("Car Workshop ver. {}", Version::get());

    let _car_workshop = CarWorkshop::new();
}
```

## Moduły podrzędne

Używanie katalogów do tworzenia modułów pomaga organizować kod, kiedy mamy do czynienia z modułami zagnieżdżonymi. Dodajmy teraz prototypy dwóch zagnieżdżonych modułów: `mechanic_manager` oraz `request_manager` - również stosując dla nich podkatalogi:

```console
$ mkdir src/car_workshop/mechanic_manager
$ touch src/car_workshop/mechanic_manager/mod.rs

$ mkdir src/car_workshop/request_manager
$ touch src/car_workshop/request_manager/mod.rs
```

W środku każdego z nich dodamy podobne, puste struktury. W pliku `mechanic_manager/mod.rs`:

```rust
pub struct MechanicManager {}

impl MechanicManager {
    pub fn new() -> Self {
        Self {}
    }
}
```

a w pliku `request_manager/mod.rs`:

```rust
pub struct RequestManager {}

impl RequestManager {
    pub fn new() -> Self {
        Self {}
    }
}
```

W nadrzędnym module `car_workshop` (plik `car_workshop/mod.rs`) użyjemy podrzędnych modułów w następujący sposób:

```rust
mod mechanic_manager;
mod request_manager;

pub use mechanic_manager::MechanicManager;
pub use request_manager::RequestManager;

pub struct CarWorkshop {
    mechanic_manager: Box<MechanicManager>,
    request_manager: Box<RequestManager>,
}

impl CarWorkshop {
    pub fn new(
        mechanic_manager: Box<MechanicManager>,
        request_manager: Box<RequestManager>,
    ) -> Self {
        Self {
            mechanic_manager,
            request_manager,
        }
    }
}
```

Dość sporo się tutaj wydażyło. W pierwszej kolejności zaimportowaliśmy podrzędne moduły:

```rust
mod mechanic_manager;
mod request_manager;
```

Następnie dodaliśmy do przestrzeni nazw struktury `MechanicManager` oraz `RequestManager`, ale słówko `use` poprzedziliśmy słówkiem `pub`. Taki zabieg ma tą cechę, że exportujemy symbol z podrzędnego modułu na zewnątrz. Jeśli ktoś zaimportuje teraz moduł `car_workshop`, to będzie miał dostęp do symbolu `MechanicManager` w taki sposób: `car_workshop::MechanicManager`, i nie będzie musiał podawać całej ścieżki `car_workshop::mechanic_manager::MechanicManager`. 

Co więcej, rozszerzyliśmy prototyp struktury `CarWorkshop` o dwa pola: 

```rust
pub struct CarWorkshop {
    mechanic_manager: Box<MechanicManager>,
    request_manager: Box<RequestManager>,
}
```

`Box<T>` to specjalna struktura z biblioteki standardowej, która wymusza umieszczenie obiektu na stercie i jednocześnie zarządza zaalokowanymi zasobami (po wyjściu z zakresu automatycznie zwolni te zasoby). (Odpowiednikiem struktury `Box<T>` w języku C++ jest `std::unique_ptr<T>`.) Ponieważ Rust nie pozwala na używanie niezainicjalizowanych zmiennych, musieliśmy także rozwinąć funkcję powiązaną `new()`:

```rust
impl CarWorkshop {
    pub fn new(
        mechanic_manager: Box<MechanicManager>,
        request_manager: Box<RequestManager>,
    ) -> Self {
        Self {
            mechanic_manager,
            request_manager,
        }
    }
}
```

Utworzenie nowego obiektu typu `CarWorkshop` wymaga przekazania mu na własność obiektów typu `MechanicManager` i `RequestManager`. Zobaczmy jak wygląda to w praktyce (plik `main.rs`):

```rust
mod version;
use version::Version;

mod car_workshop;
use car_workshop::{CarWorkshop, MechanicManager, RequestManager};

fn main() {
    println!("Car Workshop ver. {}", Version::get());

    let mechanic_manager = Box::new(MechanicManager::new());
    let request_manager = Box::new(RequestManager::new());
    let _car_workshop = CarWorkshop::new(mechanic_manager, request_manager);
}
```

Zaimportowaliśmy moduł `car_workshop` i skorzystaliśmy z wyeksportowanych symboli z modułów podrzędnych:

```rust
mod car_workshop;
use car_workshop::{CarWorkshop, MechanicManager, RequestManager};
```

W ciele funkcji `main()` dodaliśmy poprawną konstrukcję obiektu `CarWorkshop`:

```rust
    let mechanic_manager = Box::new(MechanicManager::new());
    let request_manager = Box::new(RequestManager::new());
    let _car_workshop = CarWorkshop::new(mechanic_manager, request_manager);
```

Funkcja `CarWorkshop::new()` otrzymała na własność dwa argumenty: `mechanic_manager` i `request_manager`, które zostały zaalokowane na stercie. Od teraz funkcja `CarWorkshop::new()` jest ich właścicielem i ona odpowiada za przechowywane przez nie zasoby. Po wywołaniu tej funkcji nie możemy już odwoływać się do tych dwóch zmiennych. Użyliśmy struktury `Box<T>`, ponieważ przekazywanie na własność elementów zaalokowanych na stercie jest bardzo tanie obliczeniowo - wystarczy przekazać wskaźnik.

## Uciszanie ostrzeżeń kompilatora

Gdy spróbujemy skompilować nasz program, pojawią się dwa ostrzeżenia dotyczące nieużywanych zmiennych: 

```console
warning: field is never read: `mechanic_manager`
 --> src/car_workshop/mod.rs:8:5
  |
8 |     mechanic_manager: Box<MechanicManager>,
  |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  |
  = note: `#[warn(dead_code)]` on by default

warning: field is never read: `request_manager`
 --> src/car_workshop/mod.rs:9:5
  |
9 |     request_manager: Box<RequestManager>,
  |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
```

Zwróć uwagę, że ostrzeżenie takie nie pojawiło się przy zmiennej `_car_workshop` w funkcji `main()`. Dzieje się tak, ponieważ podkreślnik (`_`) poprzedzający nazwę zmiennej tłumi takie ostrzeżenie. Aby świadomie wyciszyć ostrzeżenia tego typu możemy dodać atrybut `#![allow(dead_code)]` w pliku `main.rs` (wówczas wyciszenie dotyczy całego projektu) lub dodać atrybut #[allow(dead_code)] tuż przed miejscem, które chcemy wyciszyć:

```rust
#[allow(dead_code)]
pub struct CarWorkshop {
    mechanic_manager: Box<MechanicManager>,
    request_manager: Box<RequestManager>,
}
```

Teraz kompilacja powinna przebiegać bez zakłóceń.

## Implementacja cechy Drop

Gdy chcemy wykonać jakieś działanie w momencie gdy instancja danego typu kończy swój żywot (wychodzi z zakresu), musimy zaimplementować dla tego typu cechę `Drop`. Jest to cecha biblioteki standardowej, która została dołączona w preludium, więc nie musimy pisać pełnej ścieżki, tj. `std::ops::Drop`. Zaimplementujmy tą cechę dla struktury `CarWorkshop`:

```rust
impl Drop for CarWorkshop {
    fn drop(&mut self) {
        println!("Car Workshop exit");
    }
}
```

Zobaczmy to w akcji:

```console
$ cargo run
   Compiling car-workshop v0.2.0 (/home/osboxes/Workspace/rust-tutorial/code/car-workshop)
    Finished dev [unoptimized + debuginfo] target(s) in 0.25s
     Running `target/debug/car-workshop`
Car Workshop ver. 0.2.0
Car Workshop exit
```

Sygnatura funkcji `drop()` pożycza instancję typu `CarWorkshop` w sposób modyfikowalny, ponieważ ta i tak za chwilę zostanie zniszczona, więc na końcu możemy podjąć wszelkie istotne działania w celu czystego wyczyszczenia zasobów. Moglibyśmy tutaj np. zapisać na dysk wszelkie zmiany w systemie, które dałoby się odtworzyć po ponownym uruchomieniu. 

Myślę, że możemy podbić wersję w pliku `Cargo.toml` i przejść do dalszej implementacji.

```ini
[package]
name = "car-workshop"
version = "0.3.0"
edition = "2021"
```
