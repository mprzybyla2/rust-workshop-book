# Konfiguracja nowego projektu

Projekty zaczynane od podstaw to coś, co programiści lubią najbardziej. 

Rozpoczniemy od przygotowania struktury katalogów projektu. W tym celu skorzystamy z `cargo`.

```console
$ cd Workspace
$ cargo new car-workshop
     Created binary (application) `car-workshop` package
```

`cargo` utworzyło dla nas nowy katalog `car-workshop`, a w nim plik `Cargo.toml`, który zawiera meta-dane dotyczące naszego projektu (nazwa, wersja, edycja Rusta, zależności) oraz katalog `src/`, w którym jest już plik `main.rs` z aplikacją typu _Hello, World!_. Co więcej, `cargo` zainicjowało repozytorium Git i dodało plik `.gitignore`, który wskazuje, że docelowego katalogu, w którym budowane są pliki wynikowe (`target/`), nie należy uwzględniać w systemie kontroli wersji.

Sprawdźmy, czy nasz przykładowy projekt się kompiluje:

```console
$ cd car-workshop
$ cargo build
   Compiling car-workshop v0.1.0 (/home/osboxes/car-workshop)
    Finished dev [unoptimized + debuginfo] target(s) in 1.11s
```

Sprawdźmy też, czy możemy go uruchomić:

```console
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.02s
     Running `target/debug/car-workshop`
Hello, world!
```

Polecenie `run` kompiluje projekt przed uruchomieniem. 

Domyślnym wynikiem kompilacji jest plik zawierający wszystkie symbole do debugowania. 

Gdybyśmy chcieli skompilować projekt w trybie zoptymalizowanym, należy użyć:

```console
$ cargo build --release
   Compiling car-workshop v0.1.0 (/home/osboxes/car-workshop)
    Finished release [optimized] target(s) in 0.22s
```

Przejdźmy zatem do implementacji!
