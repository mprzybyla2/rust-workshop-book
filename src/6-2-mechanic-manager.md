# Menedżer mechaników

Zajmiemy się teraz implementacją menedżera mechaników pracujących w warsztacie. Mechanicy mogą być zatrudniani, zwalniani, a także przydzielani do konkretnych zadań.

Rozpocznijmy od zdefiniowania struktury reprezentującej konkretnego mechanika. W tym celu utwórzmy plik `src/car_workshop/mechanic_manager/mechanic.rs`. Ograniczymy się do pewnego minimum:

```rust
pub type MechanicId = u32;

pub struct Mechanic {
    pub id: MechanicId,
    pub name: String,
}
```

Mechanik ma imię i pewien unikatowy identyfikator, który przydzielany jest mu przez system. Imię ma typ `std::string::String`, który jest w preludium biblioteki standardowej, dlatego możemy zapisać po prostu `String`. Jest to ciąg znaków zgodny z UTF-8 (4 bajty na znak), który alokowany jest na stercie. Dla zwiększenia czytelności kodu, dodaliśmy alias typu `MechanicId`. Alias nie daje żadnego zabezpieczenia i możemy stosować go zamiennie z typem `u32`.

Zwróć uwagę, że przy polach struktury dodaliśmy słówko `pub`. Domyślnie pola struktury są prywatne dla kodu spoza modułu, dlatego musimy jawnie zadeklarować je jako publiczne.

Teraz skorzystamy z tej struktury w module `mechanic_manager` (`src/car_workshop/mechanic_manager/mod.rs`):

```rust
mod mechanic;
use mechanic::Mechanic;

type RequestId = u32;

pub struct MechanicManager {
    available_mechanics: Vec<Mechanic>,
    busy_mechanics: Vec<(Mechanic, RequestId)>,
}

impl MechanicManager {
    pub fn new() -> Self {
        Self {
            available_mechanics: Vec::new(),
            busy_mechanics: Vec::new(),
        }
    }
}
```

Rozszerzyliśmy strukturę `MechanicManager` o dwie listy: listę wolnych mechaników (`available_mechanics`) oraz listę zajętych mechaników (`busy_mechanics`). Obie listy korzystają z generycznej struktury `Vec`, czyli dynamicznie alokowanego wektora, reprezentującego ciągły segment pamięci. Lista wolnych mechaników zawiera instancję struktury `Mechanic`, a lista zajętych mechaników zawiera pary `Mechanic` - `RequestId`, który jest aliasem reprezentującym unikatowy numer zgłoszenia, do którego dany mechanik został przydzielony.

Żadne zmienne nie mogą pozostać niezainicjalizowane. Musieliśmy uwzględnić ten fakt w funkcji `new()`, tworząc puste wektory. W tym momencie wprowadzimy coś, co powinno towarzyszyć nam od samego początku - testy jednostkowe. Pisanie testów jednostkowych pomaga uodpornić proces rozwoju kodu źródłowego na powstawanie błędów, wynikających ze zmian dokonywanych w dalszych etapach pracy. Rust pozwala na pisanie testów w tym samym pliku źródłowym, co znacząco ułatwia sam proces testowania.

Napiszmy zatem pierwszy test, który sprawdzi, czy nowo utworzona instancja `MechanicManager` ma poprawnie zainicjalizowane pola. Test ten dodamy na samym spodzie pliku `mechanic_manager/mod.rs`:

```rust
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let manager = MechanicManager::new();
        assert_eq!(manager.available_mechanics.len(), 0);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }
}
```

Trochę się tu dzieje. Przede wszystkim definiujemy nowy moduł `test`, któremu nadajemy specjalny atrybut `#[cfg(test)]`. Atrybut ten podpowiada, że kod ten należy budować tylko gdy używamy `cargo test`, a nie należy go budować gdy używamy `cargo run` lub `cargo build`.

Wewnątrz modułu `test` użyliśmy zapisu `use super::*`. Dzięki niemu, wewnątrz modułu `test` dostępne będą wszystkie symbole (`*`) z nadrzędnego modułu (`super`), czyli z modułu `mechanic_manager`.

Dodaliśmy funkcję wolnostojącą `test_new()`, okraszoną atrybutem `#[test]`. Atrybut `#[test]` podpowiada, że funkcja ta reprezentuje przypadek testowy, który powinien zostać uruchomiony podczas wywołania `cargo test`. Wewnątrz funkcji sprawdzamy za pomocą makra `assert_eq!()`, czy pola nowo utworzonego obiektu mają odpowiednie właściwości. Jeżeli argumenty tego makra nie będę sobie równe, test nie przejdzie. Zwróć uwagę, że pola `MechanicManager` nie zostały określone jako publiczne, a mimo to mamy do nich dostęp w module `test`. Dzieje się tak, ponieważ dzieci danego modułu mają również dostęp do prywatnych elementów rodzica.

Sprawdźmy jak zadziała to w praktyce:

```console
$ cargo test
    Finished test [unoptimized + debuginfo] target(s) in 0.00s
     Running unittests (target/debug/deps/car_workshop-f924904dc56f4cf8)

running 1 test
test car_workshop::mechanic_manager::test::test_new ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
```

Test przeszedł. Uzbrojeni w tą metodę dodajmy od razu dwa kolejne testy nawet zanim napiszemy logikę, którą chcemy testować:

```rust
    #[test]
    fn test_hire() {
        let mut manager = MechanicManager::new();
        manager.hire(String::from("John Doe"));
        assert_eq!(manager.available_mechanics.len(), 1);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }

    #[test]
    fn test_fire() {
        let mut manager = MechanicManager::new();
        manager.available_mechanics.push(Mechanic {
            id: 17,
            name: String::from("Benny Hill"),
        });
        manager.fire(17);
        assert_eq!(manager.available_mechanics.len(), 0);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }
```

Są to testy funkcji, które pozwalają na dodawanie lub usuwanie mechaników z listy. Teraz zajmiemy się implementacją tych funkcji. Rozpoczniemy następująco:

```rust
    pub fn hire(&mut self, name: String) {
        self.available_mechanics.push(Mechanic { id: 0, name });
    }

    pub fn fire(&mut self, id: MechanicId) {
        for (index, mechanic) in self.available_mechanics.iter().enumerate() {
            if mechanic.id == id {
                self.available_mechanics.swap_remove(index);
                return;
            }
        }
    }
```

Chwilowo zignorujemy fakt, że każdy nowy mechanik będzie miał identyfikator `0`. Przeanalizujmy funkcję `fire()`. Pętle `for` w języku Rust działają na iteratorach (strukturach posiadających metodę `next()`). Iterator wektora wyłuskujemy za pomocą metody `iter()`, ale dodatkowo modyfikujemy go za pomocą metody `enumerate()` tak, aby oprócz referencji na obiekty typu `Mechanic` metoda `next()` zwracała także indeks elementu. Całość zwracana jest w postaci krotki, którą destrukturyzujemy do `(index, mechanic)`. 

Wewnątrz pętli sprawdzamy, czy identyfikator się zgadza i jeśli tak - usuwamy obiekt z listy. (Metoda `swap_remove()` jest szybsza od metody `remove()`, ponieważ w miejsce usuniętego obiektu wstawia ostatni element z listy, a nie przesuwa wystkie następujące po nim elementy o jedno miejsce w lewo.)

Obie metody mogą modyfikować obiekt `MechanicManager`, dlatego otrzymują parametr `$mut self`. Może się wydawać, że nastąpiło tutaj pogwałcenie zasad pożyczania: pętla `for` _pożycza_ w sposób niemodyfikowalny pole `available_mechanics` (poprzez pobranie jego iteratora), a w jej wnętrzu modyfikujemy to pole! Wygląda to na typowy aliasing. Sęk w tym, że po modyfikacji pojawiło się słówko `return`, więc Rust wie, że oba pożyczenia nie zostaną już więcej użyte, dlatego jest to dozwolone (i bezpieczne!).

Gdybyśmy wykomentowali słówko `return` i spróbowali zbudować nasz program, otrzymalibyśmy taki błąd:

```console
error[E0502]: cannot borrow `self.available_mechanics` as mutable because it is also borrowed as immutable
  --> src/car_workshop/mechanic_manager/mod.rs:26:17
   |
24 |         for (index, mechanic) in self.available_mechanics.iter().enumerate() {
   |                                  -------------------------------------------
   |                                  |
   |                                  immutable borrow occurs here
   |                                  immutable borrow later used here
25 |             if mechanic.id == id {
26 |                 self.available_mechanics.swap_remove(index);
   |                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ mutable borrow occurs here
```

A zatem ponowny cykl pętli spowodowałby, że użylibyśmy referencji na coś, co w poprzedniej pętli zostało zmodyfikowane. To mogłoby prowadzić do UB (ang. Undefined Behavior).

Rozszerzmy funkcję `fire()` tak, aby sprawdzała ona również listę pracowników przypisanych do zleceń. Zacznijmy od zmodyfikowania testu.

```rust
    #[test]
    fn test_fire() {
        let mut manager = MechanicManager::new();
        manager.available_mechanics.push(Mechanic {
            id: 17,
            name: String::from("Benny Hill"),
        });
        manager.busy_mechanics.push((
            Mechanic {
                id: 13,
                name: String::from("Max Payne"),
            },
            25,
        ));

        let request_id = manager.fire(121);
        assert_eq!(request_id, None);
        assert_eq!(manager.available_mechanics.len(), 1);
        assert_eq!(manager.busy_mechanics.len(), 1);

        let request_id = manager.fire(17);
        assert_eq!(request_id, None);
        assert_eq!(manager.available_mechanics.len(), 0);

        let request_id = manager.fire(13);
        assert_eq!(request_id, Some(25));
        assert_eq!(manager.busy_mechanics.len(), 0);
    }
```

Pisząc test zdaliśmy sobie sprawę, że jeśli zwolnimy pracownika, który był przydzielony do jakiegoś zadania, to wypadałoby uwzględnić tą informację w części kodu, która będzie odpowiadała za zarządzanie zleceniami. Dlatego założyliśmy, że funkcja `fire()` powinna zwracać typ `Option<RequestId>`. Dodatkowo przetestowaliśmy przypadek, gdy próbujemy usunąć z listy mechanika o nieistniejącym identyfikatorze.

Zwróć uwagę na powielenie instrukcji `let request_id = ...`. Jest to tzw. zacienianie zmiennej (ang. shadowing). Ma ono bardzo praktyczne znaczenie i w dużej mierze oszczędza czas, który inaczej musielibyśmy poświęcić na wymyślanie nowych zmiennych. 

A oto nowa implementacja metody `fire()`:

```rust
    pub fn fire(&mut self, id: MechanicId) -> Option<RequestId> {
        for (index, mechanic) in self.available_mechanics.iter().enumerate() {
            if mechanic.id == id {
                self.available_mechanics.swap_remove(index);
                return None;
            }
        }
        for (index, (mechanic, request_id)) in self.busy_mechanics.iter().enumerate() {
            if mechanic.id == id {
                let request_id = *request_id;
                self.busy_mechanics.swap_remove(index);
                return Some(request_id);
            }
        }
        None
    }
```

Zwróć uwagę na fakt, że iterator uzyskany przez metodę `iter()` pozwala na przeglądanie elementów kolekcji poprzez niemodyfikującą referencję. Zarówno zmienna `mechanic` jak i zmienna `request_id`, występujące w zagnieżdżonej krotce pętli `for`, mają typ referencji. Dlatego przed usunięciem mechanika z listy musimy zrobić sobie kopię `request_id`. Typ `RequestId` to tak naprawdę typ `u32`, który implementuję cechę `Copy`, dlatego do zrobienia kopii wystarczy dereferencja.

A teraz spróbujemy to samo zaimplementować w sposób bardziej _funkcyjny_.

```rust
    pub fn fire(&mut self, id: MechanicId) -> Option<RequestId> {
        self.available_mechanics
            .iter()
            .position(|mechanic| mechanic.id == id)
            .and_then(|index| {
                self.available_mechanics.swap_remove(index);
                None
            })
            .or_else(|| {
                self.busy_mechanics
                    .iter()
                    .position(|(mechanic, _)| mechanic.id == id)
                    .and_then(|index| {
                        let request_id = self.busy_mechanics[index].1;
                        self.busy_mechanics.swap_remove(index);
                        Some(request_id)
                    })
            })
    }
```

Jak widać, kombinacja iteratorów oraz typu `Option<T>` daje sporo możliwości. Pozostaje nam teraz jeszcze zajęcie się generowaniem identyfikatorów. Spróbuj samodzielnie dodać nowy moduł `id_generator`, w którym dodasz:

- strukturę `IdGenerator`
- funkcję powiązaną `new()`, która inicjalizuje nowy obiekt,
- metodę `get_new_id()`, która zwraca każdorazowo unikatowy identyfikator typu `u32` (np. przez inkrementację).
- testy jednostkowe obu funkcji.

Następnie rozszerz strukturę `MechanicManager` o pole typu `IdGenerator` i użyj go w metodzie `hire()`.

<details>
  <summary>Rozwiązanie</summary>

Plik `mechanic_manager/id_generator.rs`
```rust
pub struct IdGenerator {
    id: u32,
}

impl IdGenerator {
    pub fn new() -> Self {
        IdGenerator { id: 0 }
    }

    pub fn get_new_id(&mut self) -> u32 {
        self.id += 1;
        self.id
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let generator = IdGenerator::new();
        assert_eq!(generator.id, 0);
    }

    #[test]
    fn test_get_new_id() {
        let mut generator = IdGenerator::new();
        generator.get_new_id();
        assert_eq!(generator.id, 1);

        generator.get_new_id();
        generator.get_new_id();
        generator.get_new_id();
        assert_eq!(generator.id, 4);
    }
}
```

Plik `mechanic_manager/mod.rs`:
```rust
mod id_generator;
mod mechanic;

use id_generator::IdGenerator;
use mechanic::{Mechanic, MechanicId};

type RequestId = u32;

pub struct MechanicManager {
    available_mechanics: Vec<Mechanic>,
    busy_mechanics: Vec<(Mechanic, RequestId)>,
    id_generator: IdGenerator,
}

impl MechanicManager {
    pub fn new() -> Self {
        Self {
            available_mechanics: Vec::new(),
            busy_mechanics: Vec::new(),
            id_generator: IdGenerator::new(),
        }
    }

    pub fn hire(&mut self, name: String) {
        self.available_mechanics.push(Mechanic {
            id: self.id_generator.get_new_id(),
            name,
        });
    }

    pub fn fire(&mut self, id: MechanicId) -> Option<RequestId> {
        self.available_mechanics
            .iter()
            .position(|mechanic| mechanic.id == id)
            .and_then(|index| {
                self.available_mechanics.swap_remove(index);
                None
            })
            .or_else(|| {
                self.busy_mechanics
                    .iter()
                    .position(|(mechanic, _)| mechanic.id == id)
                    .and_then(|index| {
                        let request_id = self.busy_mechanics[index].1;
                        self.busy_mechanics.swap_remove(index);
                        Some(request_id)
                    })
            })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_new() {
        let manager = MechanicManager::new();
        assert_eq!(manager.available_mechanics.len(), 0);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }

    #[test]
    fn test_hire() {
        let mut manager = MechanicManager::new();

        manager.hire(String::from("John Doe"));
        assert_eq!(manager.available_mechanics.len(), 1);
        assert_eq!(manager.busy_mechanics.len(), 0);
    }

    #[test]
    fn test_fire() {
        let mut manager = MechanicManager::new();
        manager.available_mechanics.push(Mechanic {
            id: 17,
            name: String::from("Benny Hill"),
        });
        manager.busy_mechanics.push((
            Mechanic {
                id: 13,
                name: String::from("Max Payne"),
            },
            25,
        ));

        let request_id = manager.fire(121);
        assert_eq!(request_id, None);
        assert_eq!(manager.available_mechanics.len(), 1);
        assert_eq!(manager.busy_mechanics.len(), 1);

        let request_id = manager.fire(17);
        assert_eq!(request_id, None);
        assert_eq!(manager.available_mechanics.len(), 0);

        let request_id = manager.fire(13);
        assert_eq!(request_id, Some(25));
        assert_eq!(manager.busy_mechanics.len(), 0);
    }
}
```

</details>

Ponieważ kompilator rzuca wiele ostrzeżeń o nieużywanych częściach kodu, na czas rozwijania oprogramowania użyjemy w pliku `main.rs` atrybutu `#![allow(dead_code)]`. Atrybut ten wyciszy tego typu ostrzeżenia w całym projekcie.

Czas podbić wersję i ruszać dalej.

```ini
[package]
name = "car-workshop"
version = "0.4.0"
edition = "2021"
```
