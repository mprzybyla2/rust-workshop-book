# Podstawy języka Rust

[Podstawy języka Rust](./index.md)
- [Dlaczego Rust?](./1-why-rust.md)
- [Opis szkolenia](./2-workshop-description.md)
- [Konfiguracja środowiska](./3-system-setup.md)
- [Konfiguracja nowego projektu](./4-fresh-project.md)
- [Wyświetlanie wersji](./5-printing-version.md)
- [Logika biznesowa](./6-business-logic.md)
   - [Prototypy](./6-1-prototypes.md)
   - [Menedżer mechaników](./6-2-mechanic-manager.md)
   - [Menedżer zleceń](./6-3-request-manager.md)
