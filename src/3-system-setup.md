# Konfiguracja środowiska

Każdy biorący udział w szkoleniu może przygotować środowisko do programowania w Rust zgodnie ze swoimi upodobaniami. Poniżej zebrano szereg kroków jak przygotować przykładowe środowisko.

## Maszyna wirtualna

Należy zainstalować na komputerze [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads) (w trakcie pisania tego skryptu jest to wersja 6.1). Następnie należy pobrać obraz maszyny wirtualnej z zainstalowanym systemem [Ubuntu 21.04 (Hirsute Hippo)](https://www.osboxes.org/ubuntu/). Domyślny użytkownik to `osboxes`, a hasło to `osboxes.org`.

Po uruchomieniu obrazu maszyny wirtualnej warto wykonać kilka dodatkowych czynności:

- "Urządzenia -> Zamontuj obraz płyty z dodatkami gościa..." - należy zatwierdzić wszystko o co zostaniemy zapytani.
- "Urządzenia -> Wspólny schowek -> Bidirectional" - należy zresetować maszynę wirtualną.

Jeśli chcemy korzystać ze współdzielonego folderu pomiędzy maszyną wirtualną a hostem należy skonfigurować taki folder w ustawieniach maszyny wirtualnej, a po jej uruchomieniu należy dodać użytkownika do grupy `vboxsf` (wciśnij `Ctrl + Alt + T` aby otworzyć terminal):

```console
$ sudo adduser $USER vboxsf
```

Po dodaniu użytkownika do grupy należy wylogować i ponownie zalogować się do systemu.

W katalogu domowym warto utworzyć nowy katalog `Workspace/`, w którym przechowywane będą pisane projekty.

```console
$ cd 
$ mkdir Workspace
```

## Aktualizacja systemu

W pierwszej kolejności należy zaktualizować istniejące oprogramowanie na maszynie wirtualnej. W tym celu w terminalu wpisujemy kolejno:

```console
$ sudo apt update
$ sudo apt upgrade
$ sudo apt autoremove 
$ reboot
```

## Instalacja dodatkowego oprogramowania

Instalujemy kilka dodatkowych pakietów:

```console
$ sudo apt install curl git build-essential
```

Instalujemy kompilator języka Rust zgodnie z instrukcją z [oficjalnej strony](https://www.rust-lang.org/tools/install):

```console
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Podczas instalacji należy użyć domyślnej konfiguracji

```console
1) Proceed with installation (default)
```

Po zainstalowaniu przeładowujemy konfigurację powłoki i sprawdzamy, czy kompilator poprawnie się zainstalował:

```console
$ source $HOME/.cargo/env
$ rustc --version
rustc 1.59.0 (9d1b2106e 2022-02-23)
```

Instalujemy dodatkowe komponenty do integracji z IDE:

```console
$ rustup component add rls rust-analysis rust-src
```

## Instalacja IDE

```console
$ sudo snap install --classic code
code e18005f0 from Visual Studio Code (vscode✓) installed
```

W VS Code należy zainstalować rozszerzenie "Rust".
