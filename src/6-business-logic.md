# Logika biznesowa

Logika biznesowa to ta część kodu, która mówi **co** ma się dziać, w przeciwieństwie do szczegółów implementacyjnych, które mówią **jak** to ma się dziać. Logika biznesowa wynika przede wszystkim z wymagań postawionych na początku oraz w trakcie trwania projektu. 
