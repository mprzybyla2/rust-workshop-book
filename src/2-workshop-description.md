# Opis szkolenia

Podczas warsztatów będziemy starali się zaimplementować serwis, który ma działać jako backend dla wymyślonego warsztatu samochodowego. Backend ten ma za zadanie zarządzać zgłoszeniami napraw samochodowych, przydzielaniem do tych napraw mechaników samochodowych oraz odpowiednich zestawów narzędzi. 

Podczas szkolenia będziemy korzystać ze skryptu, który właśnie czytasz. Są w nim dostępne próbki kodu, które łatwo można przeklejać do własnego edytora. Na końcu każdego rozdziału będzie też dostępny do pobrania plik `.zip`, w którym będzie znajdował się kompletny projekt na danym etapie rozwoju.
